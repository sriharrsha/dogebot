import {CoinbasePro, WebSocketChannelName, WebSocketEvent, CandleGranularity, Candle} from 'coinbase-pro-node';
import * as path from 'path';
import * as fs from 'fs';
import {indicators} from 'tulind';

let pairData: Candle[] = [];

// API Keys can be generated here:
// https://pro.coinbase.com/profile/api
// https://public.sandbox.pro.coinbase.com/profile/api
const auth = {
    apiKey: "08f0f086ad4d8b822dee7c678150f4e8",
    apiSecret: "Ov9TLRSSgmSUkkWekX2pQef+kmZkDbgu2qHXw2B8c+WPQNwrvmjSE3o+sJXcr1knFeiDJOQE7j7lcar8sucSow==",
    passphrase: "avpg2zltz3i",
    // The Sandbox is for testing only and offers a subset of the products/assets:
    // https://docs.pro.coinbase.com/#sandboxcle
    useSandbox: true,
};
const client = new CoinbasePro(auth);

function coinbaseCheck(){
    
    client.rest.account.listAccounts().then(accounts => {
        const message = `You can trade "${accounts.length}" different pairs.`;
        console.log(message);
    }).catch((any)=>{
        console.log(any);    
});
}

//Initialize File Dump

async function dump(): Promise<void> {
    const productId = 'BTC-USD';
    const begin = '2020-04-11T00:00:00.000Z';
    const end = new Date().toISOString();
    const granularity = CandleGranularity.FIVE_MINUTES;
    const directory = __dirname;

    const candles = await client.rest.product.getCandles(productId, {
      end,
      granularity,
      start: begin,
    });
  
    pairData = candles;
    const start = candles[0].openTimeInMillis;
    const file = path.join(directory, `${productId}-${start}-${granularity}.json`);
  
    fs.writeFileSync(file, JSON.stringify(candles, null, 2));
  
    console.info(`Dumped "${candles.length}" candles in file "${file}".`);
}


function loadDataFromFile(){
    pairData = JSON.parse(fs.readFileSync('./BTC-USD-1586563200000-300.json', 'utf8'));
    // for(let item of pairData){
    //     console.log(item);
    //     // pairData.push(item as Candle);
    // }
}  


function calculateSMA(size: number){
    //Do a simple moving average on close prices with period of 3.
    const close: number[] = pairData.map(candle => candle.close); 
    indicators.sma.indicator( [close], [size], function(err, results) {
        console.log("Result of sma is:");
        console.log(results[0]);
    });
}


loadDataFromFile();
calculateSMA(30);
